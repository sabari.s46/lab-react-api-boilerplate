import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    axios
      .get("https://reactnd-books-api.udacity.com/books", {
        headers: { Authorization: "whatever-you-want" },
      })
      .then((result) => {
        console.log(result.data.books);
        setBooks(result.data.books);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);

  return (
    <div className="App">
      {books.map((book) => (
        <div key={book.id} className="boss">
          <h1>{book.title}</h1>
          <div className="bookdet">
            <img src={book.imageLinks.smallThumbnail} alt="" />
            <p>{book.description}</p>
          </div>
          <p>{book.authors}</p>
          <hr />
        </div>
      ))}
      {/* <hr /> */}
    </div>
  );
}

export default App;
